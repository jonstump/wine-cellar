import React from "react";
import Wine from "./Wine";
import PropTypes from "prop-types";

function WineKegList(props){
  return (
    <React.Fragment>
      <hr/>
      {props.wineList.map((wine) =>
        <Wine
          whenWineClicked = { props.onWineSelection }
          name={wine.name}
          varietal={wine.varietal}
          brand={wine.brand}
          region={wine.region}
          alcoholContent={wine.alcoholContent}
          price={wine.price}
          pints={wine.pints}
          id={wine.id}
          key={wine.id}/>
      )}
    </React.Fragment>
  );
}

WineKegList.propTypes = {
  wineList: PropTypes.array,
  onWineSelection: PropTypes.func
};

export default WineKegList;
