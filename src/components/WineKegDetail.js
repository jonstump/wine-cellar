import React from 'react'
import PropTypes from "prop-types";

function WineKegDetail(props){
  const { wine } = props;

  return (
    <>
      <h1>Wine Keg Details</h1>
      <h3>{wine.name}</h3>
      <p><em>Pints Remaining:{wine.pints}</em></p>
      <p>{wine.varietal}</p>
      <p>{wine.region}</p>
      <p>{wine.brand}</p>
      <p>{wine.price}</p>
      <p>Alcohol Content:{wine.alcoholContent}</p>
      <button className='btn btn-secondary' onClick={ props.onClickingEdit }>Update Wine</button>
      <button className='btn btn-danger' onClick={ props.onClickingDelete }>Clear Keg</button>
      {/* <button className='btn btn-success' onClick={ handlePintDecreaseButton }>Pint Sold</button> */}
    </>
  );
}

WineKegDetail.propTypes = {
  wine: PropTypes.object,
  onClickingEdit: PropTypes.func,
  onClickingDelete: PropTypes.func,
  onPintDecrease: PropTypes.func
};

export default WineKegDetail
