import React from "react";
import PropTypes from "prop-types";

function PintDecreseButton (props) {
  const { wine } = props;

  function handlePintDecreaseButton(event) {
    event.preventDefault();
    if (wine.pints > 0) {
    props.onPintDecrease({pints: wine.pints - 1,
                          name: wine.name,
                          varietal: wine.varietal,
                          brand: wine.brand,
                          price: wine.price,
                          alcoholContentl: wine.alcoholContent,
                          id: wine.id});
    }else{
      //need to insert code for a warning message here.
    }
  }
  return (
  <>
    <button className='btn btn-success' onClick={ handlePintDecreaseButton }>Pint Sold</button>
  </>
  )
}

PintDecreseButton.propTypes = {
  wine: PropTypes.object,
  onPintDecrease: PropTypes.func
};

export default PintDecreseButton;
