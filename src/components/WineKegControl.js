import React from 'react';
import WineKegDetail from './WineKegDetail';
import WineKegList from './WineKegList';
import NewWineForm from './NewWineForm';
import EditWineForm from './EditWineForm'
import PintDecreseButton from './PintDecerase'

class WineControl extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      formVisibleOnPage: false,
      masterWineList: [],
      selectedWine: null,
      editing: false
    };
  }

  handleAddingNewWineToList = (newWine) => {
    const newMasterWineList = this.state.masterWineList.concat(newWine);
    this.setState({masterWineList: newMasterWineList,
                  formVisibleOnPage: false });
  }

  //This code doesn't work at the moment and I am unsure why. Needs further investigation.
  handleDeletingWine = (id) => {
    const newMasterWineList = this.state.masterWineList.filter(wine => wine.id !== id);
    this.setState({
      masterWineList: newMasterWineList,
      selectedWine: null
    });
  }

  handleEditClick = () => {
    this.setState({editing: true});
  }

  handleChangingSelectedWine = (id) => {
    const selectedWine = this.state.masterWineList.filter(wine => wine.id === id)[0];
    this.setState({selectedWine: selectedWine});
  }


  handleEditingWineInList = (wineToEdit) => {
    const editedMasterWineList = this.state.masterWineList
      .filter(wine => wine.id !== this.state.selectedWine.id)
      .concat(wineToEdit);
    this.setState({
        masterWineList: editedMasterWineList,
        editing: false,
        selectedWine: null
      });
  }

  handleClick = () => {
    if (this.state.selectedWine != null) {
      this.setState({
        formVisibleOnPage: false,
        selectedWine: null,
        editing: false
      });
    } else {
      this.setState(prevState => ({
        formVisibleOnPage: !prevState.formVisibleOnPage,
      }));
    }
  }

  render(){
    let currentlyVisibleState = null;
    let buttonText = null;
    if (this.state.editing ) {
      currentlyVisibleState = <EditWineForm wine = {this.state.selectedWine} onEditWine = {this.handleEditingWineInList} />
      buttonText = "Return to Wine List";
    } else if (this.state.selectedWine != null) {
      currentlyVisibleState =
        <div>
        <WineKegDetail wine = {
      this.state.selectedWine}
      onClickingDelete = {this.handleDeletingWine}
        onClickingEdit = {this.handleEditClick}
        onPintDecrease = {this.handlePintDecrease}/>
      <PintDecreseButton wine = {this.state.selectedWine} onPintDecrease = {this.handleEditingWineInList} />
          </div>
      buttonText = "Return to Wine List";
    } else if (this.state.formVisibleOnPage) {
      currentlyVisibleState = <NewWineForm onNewWineCreation={this.handleAddingNewWineToList} />
      buttonText = "Return to Wine List";
    } else {
      currentlyVisibleState = <WineKegList wineList={this.state.masterWineList}
      onWineSelection={this.handleChangingSelectedWine} />;
      buttonText = "Add Wine";
    }
    return (
      <>
        {currentlyVisibleState}
        <button className='btn btn-primary' onClick={this.handleClick}>{buttonText}</button>
      </>
    );
  }

}

export default WineControl;
